# mySQL Partie 2#

## Back to Basics : SELECT ##

Nous allons utiliser la BDD Sakila de Mysql :

http://downloads.mysql.com/docs/sakila-db.zip

Merci de dézipper et d'importer la BDD Dans une instace de MySQL.


## Types de données

  * TinyInt -128 -> 127
  * SMALLINT -32768 -> 32767
  * INT -2147483648 -> 2147483647
  * CHAR : max 255 char.
  * CHAR BINAR : comme CHAR mais insensible à la casse.
  * VARCHAR : max 255 (mais remplis)
  * TEXT : 65535 char.
  * DATE : AAAA-MM-JJ
  * DATETIME : AAAA-MM-JJ HH:MM:SS
  * TIME : HH:MM:SS
  * TIMESTAMP : AAAAMMJJHHMMSS


## Structure d'une requete


> SELECT (indentificateur) FROM (identificateur) WHERE (conditions)

### identificateurs ###

 bdd.table.champ / bdd.table

> SELECT sakila.films.title FROM sakila.films.title WHERE sakila.films.title = "fast and furious";


### SELECT ###

Selectionner tous les champs d'une table :

> SELECT * FROM films;


Selectionner uniquement le titre et l'année du film :

> SELECT title, release_year from films;

Selectionner tous les champs d'une table, avec condition :

> SELECT * from films WHERE release_year = 2005;

Selectionner tous les champs d'une table avec plusieurs conditions:

> SELECT * from films WHERE release_year=2016 AND lenght>90;


### Actions spéciales ###

Ne choisir que les itérations d'une information multiple : 

> SELECT DISCTINCT release_year FROM films;


### ORDONNANCEMENT ###

Trier les résultats par un champ : 

> SELECT * FROM films ORDER BY title ASC;

  * ASC = ascendant / croissant
  * DESC = descendant / décrroissant

Limiter les résultats aux 'n' premiers : 

> SELECT * FROM films LIMIT 10;


### REGROUPEMENT ###

Lors de certains usages, on doit regrouper les informations (en cas de somme, moyenne, distinct).

> SELECT film_id,count(actor_id) FROM `film_actor`

va renvoyer le premier ID de la table, et le nombre d'acteurs

> SELECT film_id,count(actor_id) FROM `film_actor` GROUP BY film_id

va renvoyer la même chose, mais en regroupant le comptage par id de film.

et si on veut ordonner ça par ordre décroissant : 

> SELECT film_id,count(actor_id) FROM `film_actor` GROUP BY film_id order by count(actor_id);

Ce qui est très embêtant.

### ALIAS ###

On peut renommer les noms des champs retournés pour plus de facilité : 

> SELECT film_id as "ID DE FILM" FROM film;

Mais surtout en cas d'utilisation d'aggrégation : 

> SELECT film_id,count(actor_id) as nb_acteurs FROM `film_actor` GROUP BY film_id order by nb_acteurs;

### TPS ###

1. Sélectionner tous les acteurs dont le nom de famille est WOOD.
2. Selectionner le titre, la description et la durée tous les films dont le titre contient "scarface"
3. Les ordonner par durée décroissante.
4. Pour ces films afficher la catégorie.

5. Trouver l'ID du film TOMATOES HELLFIGHTERS
6. pour ce film, lister les acteurs.
7. Dans quels films  de moins de 80mn, visionnables par tous a également joué Warren Nolte ?

8. Afficher les films contenant "robot" dans la description, avec les acteurs, la catégorie du film et visibles aux plus de 13 ans.  



