# Cours SQL, partie 4 : On arrête de rire.

30-08-2016

## Fonctions MySQL

### Chaînes ###

On peut préparer le retour de données MySQL avec des opérations internes : 

1. Majuscules / Minuscule 

> UPPER() / LOWER()

> SELECT UPPER('en majuscule svp'); 

2. Retourner la position d'une chaîne dans une autre : 

> INSTR(str,substr)
 
>  SELECT INSTR('Rechercher une aiguille', 'aiguille');

3. Remplacer des caractères : 

> REPLACE (chaine, remplacement, a_remplacer)

> SELECT REPLACE("Les robot de l'espace","robot", "robots");


4. Revoyer les parties droites ou gauches d'une chaine

> RIGHT(chaine,nbcar) / LEFT(chaine,nbcar)

> SELECT LEFT("popschool","3");

5. Concaténation avec séparateur 

> CONCAT_WS(séparateur, exp1, exp2 ...)

SELECT CONCAT_WS(",", "champ1","champ3","champ5")



### Dates



### Fichier 

Imaginons charger une affiche de film. 
Il faut ajouter un champ blob à la table film

> ALTER TABLE films ADD poster BLOB

ensuite on peut charger un fichier en binaire dans la table : 

> UPDATE films SET poster = LOAD_FILE('/tmp/affiche.jpg') WHERE ID=1;



